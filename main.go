package main

import (
	"fmt"

	// Import the generated protobuf code
	"github.com/micro/go-micro/v2"
	pb "gitlab.com/hectorlachambre-ms/shippy-service-consignment/proto/consignment"
	vesselProto "gitlab.com/hectorlachambre-ms/shippy-service-vessel/proto/vessel"
)

func main() {

	// Create a new service. Optionally include some options here.
	srv := micro.NewService(

		// This name must match the package name given in your protobuf definition
		micro.Name("shippy.service.consignment"),
	)

	// Init will parse the command line flags.
	srv.Init()

	vesselClient := vesselProto.NewVesselService("shippy.service.vessel", srv.Client())

	repository := &Repository{}
	service := &Service{repository, vesselClient}

	// Register handler
	pb.RegisterShippingServiceHandler(srv.Server(), service)

	// Run the server
	if err := srv.Run(); err != nil {
		fmt.Println(err)
	}
}
