module gitlab.com/hectorlachambre-ms/shippy-service-consignment

go 1.14

require (
	github.com/golang/protobuf v1.3.5
	github.com/micro/go-micro/v2 v2.2.0
	github.com/micro/protoc-gen-micro/v2 v2.0.0 // indirect
	gitlab.com/hectorlachambre-ms/shippy-service-vessel v0.1.0
	golang.org/x/net v0.0.0-20200301022130-244492dfa37a // indirect
	golang.org/x/sys v0.0.0-20200302150141-5c8b2ff67527 // indirect
)
